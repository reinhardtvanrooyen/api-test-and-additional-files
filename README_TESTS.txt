******DOG API TEST******

- I used PostMan to verify the API calls to validate the correct data is returning
- I used the URL that was used in PostMan to impliment it into JMeter 3 
- I have attached screen shots of the assertion messages and calls in my API test

HOW TO RUN THE API

- Download JMeter 3 and install (I have uploaded the installation of JMeter 3 into the repository) 
-- You will also need JDK8 (I have added a JDK8 verstion that will also install Netbeans that I used for my Automation script)
- Open JMeter and import the dog_api.jmx file (naviate into the apache-jmeter-3.0 -> bin and run the jmeter.bat file []) 
- Press the Green Play button to execute the API tests

HOW TO VIEW THE API CALLS

- Drill down the menu 
-- DOG API -> Dog API Test Group 
- Click on View Results Tree to view the test results

NOTE: I have added a Summary Report and a View Results in Table for more reporting options
NOTE: I have created a global variable called site and used https://dog.ceo/api I call ${site} and then the rest of the https link.


******Add Users List******

- I used Netbeans as my IDE and JAVA as my coding language
- I have pushed my code into a public bitbucket repository 

HOW TO RUN THE ADD USERS LIST AUTOMATION TEST

- Clone the files from the repository to your local machine
- Open Netbeans and open the project from your local machine
- Drill down to KISSAT-Base -> Unit Tests -> TestSuits and double click on Add_Unique_user.java
- Right click on and click Run Focus TEST (For some unknown reason it might not launch the first 2 times you do this action, but on the 3rd time it will run)
- To view the source code KISSAT-Base -> Source -> Testing -> TestClasses and double click on AddUserList.java
- To view the stored page object KISSAT-Base -> Source -> Testing -> PageObjects and double click on AddUserListPageObjects


HOW TO VIEW THE RESULTS

- The test takes about 19 secons
- On the local project files navigate to project/KeywordDrivenTestReports in here is a report of the run with steps recorded that passed. If there are any scripting issues or bug its will note it and take a screenshot of where it went wrong and it will display in this report.


BUGS I FOUND WHILE AUTOMATING THE ADD USERS LIST

1) The added users does not save the customer radio buttons.
2) The added users does not actually get saved after refresh(I know this is turned off for spamming reasons) so the task to add a unique user after multiple runs wont ever work however I did cater for it in my code
3) When adding a new user, after adding one already, the fields are pre populated with the previous user's details
